module bitbucket.org/lygo/lygo_updater

go 1.14

require (
	bitbucket.org/lygo/lygo_commons v0.1.120
	bitbucket.org/lygo/lygo_events v0.1.10
	bitbucket.org/lygo/lygo_scheduler v0.1.13
	github.com/google/uuid v1.3.0 // indirect
)
