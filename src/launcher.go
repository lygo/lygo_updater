package lygo_updater

import (
	"bitbucket.org/lygo/lygo_commons/lygo_exec"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_events"
	"bytes"
	"os/exec"
	"strings"
)

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t a n t s
//----------------------------------------------------------------------------------------------------------------------

const (
	onQuit  = "on_quit"
	onStart = "on_start"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type Launcher struct {
	keepHandle     bool
	cmd            *exec.Cmd
	command        string
	err            error
	out            *bytes.Buffer
	pid            int
	chanCmd        chan bool // command executed
	chanQuit       chan bool // command terminated
	ended          bool
	events         *lygo_events.Emitter
	onQuitHandler  func(command string, pid int)
	onStartHandler func(command string)
}

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t r u c t o r
//----------------------------------------------------------------------------------------------------------------------

func NewLauncher(keepHandle bool) *Launcher {
	instance := new(Launcher)
	instance.keepHandle = keepHandle
	instance.chanCmd = make(chan bool, 1)
	instance.chanQuit = make(chan bool, 1)
	instance.ended = true
	instance.pid = -1
	instance.events = lygo_events.NewEmitter()

	instance.events.On(onQuit, func(event *lygo_events.Event) {
		if nil != instance && nil != instance.onQuitHandler {
			instance.onQuitHandler(event.ArgumentAsString(0), event.ArgumentAsInt(1))
		}
	})
	instance.events.On(onStart, func(event *lygo_events.Event) {
		if nil != instance && nil != instance.onStartHandler {
			instance.onStartHandler(event.ArgumentAsString(0))
		}
	})

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *Launcher) String() string {
	if nil != instance {
		return instance.GoString()
	}
	return ""
}

func (instance *Launcher) GoString() string {
	if nil != instance {
		info := map[string]interface{}{
			"pid": instance.pid,
			"out": instance.Output(),
		}
		return lygo_json.Stringify(info)
	}
	return ""
}

func (instance *Launcher) Run(command string) error {
	instance.runCommand(command)

	// wait command run
	<-instance.chanCmd

	return instance.err
}

// wait command terminated
func (instance *Launcher) Wait() error {

	// wait command terminated
	<-instance.chanQuit

	return instance.err
}

func (instance *Launcher) Kill() error {
	if nil != instance.cmd && !instance.ended {
		if nil != instance.cmd.Process {
			instance.quit(true)
			err := instance.cmd.Process.Kill()
			return err
		}
	}
	return nil
}

func (instance *Launcher) IsKillable() bool {
	if nil != instance.cmd && !instance.ended {
		if nil != instance.cmd.Process {
			return true
		}
	}
	return false
}

func (instance *Launcher) Pid() int {
	return instance.pid
}

func (instance *Launcher) Output() string {
	if nil != instance.cmd && nil != instance.out {
		return instance.out.String()
	}
	return ""
}

func (instance *Launcher) OnQuit(callback func(command string, pid int)) {
	if nil != instance {
		instance.onQuitHandler = callback
	}
}

func (instance *Launcher) OnStart(callback func(command string)) {
	if nil != instance {
		instance.onStartHandler = callback
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *Launcher) runCommand(rawCommand string) {
	instance.ended = false
	instance.command = rawCommand

	if len(rawCommand) > 0 {
		instance.events.EmitAsync(onStart, instance.command)

		tokens := strings.Split(rawCommand, " ")
		command := tokens[0]
		params := make([]string, 0)
		if len(tokens) > 1 {
			params = tokens[1:len(tokens)]
		}

		if instance.keepHandle {
			go instance.start(command, params...)
		} else {
			// run and forget: does not wait response to avoid screen lock
			go instance.runBackground(command, params...)
		}

		return
	}
	instance.chanCmd <- false
	instance.quit(false)
}

func (instance *Launcher) runBackground(command string, params ...string) {
	_, _ = lygo_exec.RunBackground(command, params...)
	// command run
	instance.chanCmd <- true
	instance.quit(true)
}

func (instance *Launcher) start(command string, params ...string) {
	cmd, buff, _ := lygo_exec.Command(command, params...)
	instance.cmd = cmd
	instance.out = buff

	// start
	err := cmd.Start()
	if nil != err {
		instance.err = err
	} else {
		instance.pid = cmd.Process.Pid
	}
	// command run
	instance.chanCmd <- true

	// wait
	if nil == err {
		err = cmd.Wait()
		if nil != err {
			instance.err = err
		}
	}

	// notify exit
	instance.quit(true)
}

func (instance *Launcher) quit(value bool) {
	pid := instance.pid
	cmd := instance.command

	instance.pid = -1
	instance.out.Reset()
	instance.ended = true
	instance.chanQuit <- value
	instance.events.EmitAsync(onQuit, cmd, pid)
}
