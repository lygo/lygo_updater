package lygo_updater

import "bitbucket.org/lygo/lygo_scheduler"

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type Settings struct {
	Uid                 string                     `json:"uid"`
	VersionFileRequired bool                       `json:"version_file_required"` // if true, first start will update all if version file does not exists
	VersionFile         string                     `json:"version_file"`
	PackageFiles        []*PackageFile             `json:"package_files"`
	CommandToRun        string                     `json:"command_to_run"`
	ScheduledUpdates    []*lygo_scheduler.Schedule `json:"scheduled_updates"`
	ScheduledRestart    []*lygo_scheduler.Schedule `json:"scheduled_restart"`
	ScheduledTasks      []*lygo_scheduler.Schedule `json:"scheduled_tasks"`
}

type PackageFile struct {
	File   string `json:"file"`
	Target string `json:"target"`
}
