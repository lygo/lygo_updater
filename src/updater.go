package lygo_updater

import (
	"bitbucket.org/lygo/lygo_commons/lygo"
	"bitbucket.org/lygo/lygo_commons/lygo_conv"
	"bitbucket.org/lygo/lygo_commons/lygo_errors"
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_regex"
	"bitbucket.org/lygo/lygo_commons/lygo_rnd"
	"bitbucket.org/lygo/lygo_events"
	"bitbucket.org/lygo/lygo_scheduler"
	"errors"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"
)

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t
//----------------------------------------------------------------------------------------------------------------------

var (
	ErrorMissingConfigurationParameter = errors.New("missing_configuration_parameter_error")
)

const (
	VariableDirHome  = "$dir_home"  // root
	VariableDirStart = "$dir_start" // binary launch dir
	VariableDirApp   = "$dir_app"   // binary dir
	VariableDirWork  = "$dir_work"  // workspace

	onUpgrade       = "on_upgrade"
	onError         = "on_error"
	onTask          = "on_task"
	onStartLauncher = "on_start_launcher"
	onStopLauncher  = "on_stop_laucher"

	DirStart = "start"
	DirApp   = "app"
	DirWork  = "*"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type GenericEventHandler func(updater *Updater, eventName string, args []interface{})
type UpdaterErrorHandler func(err string)
type UpdaterUpgradeHandler func(fromVersion, toVersion string, files []string)
type LauncherStartHandler func(command string)
type LauncherQuitHandler func(command string, pid int)
type TaskHandler func(taskUID string, payload map[string]interface{})

type Updater struct {
	root                string
	dirStart            string
	dirApp              string
	dirWork             string
	uid                 string
	settings            *Settings
	variables           map[string]string
	launcher            *Launcher
	schedulerUpdate     *lygo_scheduler.Scheduler
	schedulerRestart    *lygo_scheduler.Scheduler
	schedulerTask       *lygo_scheduler.Scheduler
	events              *lygo_events.Emitter
	genericHandlers     []GenericEventHandler
	errorHandlers       []UpdaterErrorHandler
	upgradeHandlers     []UpdaterUpgradeHandler
	launchStartHandlers []LauncherStartHandler
	launchQuitHandlers  []LauncherQuitHandler
	taskHandlers        []TaskHandler
	chanQuit            chan bool
	started             bool
	isReadyToRestart    bool // is launcher already started al least once?
	processMux          sync.Mutex
}

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t r u c t o r
//----------------------------------------------------------------------------------------------------------------------

func NewUpdater(settings ...interface{}) *Updater {
	instance := new(Updater)
	instance.uid = lygo_rnd.Uuid()
	instance.started = false
	instance.root = lygo_paths.Absolute("./")
	instance.dirStart = lygo_paths.GetWorkspace(DirStart).GetPath()
	instance.dirApp = lygo_paths.GetWorkspace(DirApp).GetPath()
	instance.dirWork = lygo_paths.GetWorkspace(DirWork).GetPath()
	instance.chanQuit = make(chan bool, 1)
	instance.events = lygo_events.NewEmitter()
	instance.variables = make(map[string]string)

	if len(settings) > 0 {
		instance.init(settings[0])
	} else {
		instance.init("./updater.json")
	}

	if nil == instance.settings {
		instance.settings = new(Settings)
		instance.settings.ScheduledUpdates = make([]*lygo_scheduler.Schedule, 0)
	}

	instance.genericHandlers = make([]GenericEventHandler, 0)
	instance.errorHandlers = make([]UpdaterErrorHandler, 0)
	instance.upgradeHandlers = make([]UpdaterUpgradeHandler, 0)
	instance.initUpdaterEvents()

	instance.launchStartHandlers = make([]LauncherStartHandler, 0)
	instance.launchQuitHandlers = make([]LauncherQuitHandler, 0)
	instance.taskHandlers = make([]TaskHandler, 0)

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *Updater) Settings() *Settings {
	if nil != instance {
		return instance.settings
	}
	return nil
}

func (instance *Updater) SetUid(uid string) {
	if nil != instance {
		instance.uid = uid
	}
}

func (instance *Updater) GetUid() string {
	if nil != instance {
		return instance.uid
	}
	return ""
}

func (instance *Updater) SetRoot(path string) {
	if nil != instance {
		instance.root = lygo_paths.Absolute(path)
	}
}

func (instance *Updater) GetRoot() string {
	if nil != instance {
		return instance.root
	}
	return ""
}

func (instance *Updater) GetDirStart() string {
	if nil != instance {
		return instance.dirStart
	}
	return ""
}

func (instance *Updater) GetDirApp() string {
	if nil != instance {
		return instance.dirApp
	}
	return ""
}

func (instance *Updater) GetDirWork() string {
	if nil != instance {
		return instance.dirWork
	}
	return ""
}

func (instance *Updater) SetVariable(name, value string) {
	if nil != instance && nil != instance.variables {
		if strings.Index(name, "$") == -1 {
			name = "$" + name
		}
		instance.variables[name] = value
	}
}

func (instance *Updater) GetVariable(name string) string {
	if nil != instance && nil != instance.variables {
		if strings.Index(name, "$") == -1 {
			name = "$" + name
		}
		if v, b := instance.variables[name]; b {
			return v
		}
	}
	return ""
}

func (instance *Updater) OnEvent(handler GenericEventHandler) {
	if nil != instance && nil != handler {
		instance.genericHandlers = append(instance.genericHandlers, handler)
	}
}

func (instance *Updater) OnError(handler UpdaterErrorHandler) {
	if nil != instance && nil != handler {
		instance.errorHandlers = append(instance.errorHandlers, handler)
	}
}

func (instance *Updater) OnUpgrade(handler UpdaterUpgradeHandler) {
	if nil != instance && nil != handler {
		instance.upgradeHandlers = append(instance.upgradeHandlers, handler)
	}
}

func (instance *Updater) OnTask(handler TaskHandler) {
	if nil != instance && nil != handler {
		instance.taskHandlers = append(instance.taskHandlers, handler)
	}
}

func (instance *Updater) OnLaunchStart(handler LauncherStartHandler) {
	if nil != instance && nil != handler {
		instance.launchStartHandlers = append(instance.launchStartHandlers, handler)
	}
}

func (instance *Updater) OnLaunchQuit(handler LauncherQuitHandler) {
	if nil != instance && nil != handler {
		instance.launchQuitHandlers = append(instance.launchQuitHandlers, handler)
	}
}

func (instance *Updater) HasUpdates() bool {
	currentVersion, remoteVersion, _ := instance.getVersions()
	return instance.needUpdate(currentVersion, remoteVersion)
}

func (instance *Updater) IsUpgradable(currentVersion, remoteVersion string) bool {
	return instance.needUpdate(currentVersion, remoteVersion)
}

func (instance *Updater) IsProcessRunning() bool {
	if nil != instance && nil != instance.launcher {
		instance.processMux.Lock()
		defer instance.processMux.Unlock()

		return instance.launcher.Pid() > -1
	}
	return false
}

func (instance *Updater) GetProcessOutput() string {
	if nil != instance && instance.IsProcessRunning() {
		return instance.launcher.Output()
	}
	return ""
}

func (instance *Updater) GetProcessPid() int {
	if nil != instance && nil != instance.launcher {
		instance.processMux.Lock()
		defer instance.processMux.Unlock()

		return instance.launcher.Pid()
	}
	return -1
}

// Start start
func (instance *Updater) Start() (updated bool, fromVersion string, toVersion string, files []string, err error) {
	if nil != instance {
		instance.processMux.Lock()
		defer instance.processMux.Unlock()

		instance.started = true
		instance.refreshVariables()

		// is launcher active?
		if nil != instance.launcher {
			if instance.HasUpdates() {
				instance.stopLauncher()
			}
		}

		// check updates and download
		updated, fromVersion, toVersion, files, err = instance.checkUpdates()

		if len(instance.settings.CommandToRun) > 0 {
			// LAUNCH PROGRAM
			launchErr := instance.startLauncher()
			if nil != launchErr {
				instance.events.EmitAsync(onError, launchErr.Error())
				if nil == err {
					err = launchErr
				}
			}
		}

		if nil != err {
			instance.events.EmitAsync(onError, err.Error())
		}
		if updated {
			instance.events.EmitAsync(onUpgrade, fromVersion, toVersion, files)
		}

		// START SCHEDULER IF ANY AND IF NOT STARTED YET
		// schedulerUpdate should always start because update server may be down,
		// but updater should continue to check for new updates
		instance.initScheduler()
	}
	return updated, fromVersion, toVersion, files, err
}

func (instance *Updater) Stop() {
	if nil != instance {
		instance.processMux.Lock()
		defer instance.processMux.Unlock()

		if nil != instance.launcher {
			_ = instance.launcher.Kill() // STOP RUNNING PROGRAM
		}
		instance.started = false
		instance.chanQuit <- true
	}
}

func (instance *Updater) Wait() {
	if nil != instance {
		if !instance.started {
			_, _, _, _, _ = instance.Start()
		}
		<-instance.chanQuit // wait exit
		instance.chanQuit = make(chan bool, 1)
	}
}

func (instance *Updater) ReStart() {
	if nil != instance && nil != instance.launcher {
		instance.processMux.Lock()
		defer instance.processMux.Unlock()

		if !instance.isReadyToRestart {
			instance.isReadyToRestart = true
		} else {
			// temporary stop for updater scheduler
			instance.schedulerUpdate.Pause()
			defer instance.schedulerUpdate.Resume()

			// stop the launcher
			instance.stopLauncher()

			// start launcher again
			launchErr := instance.startLauncher()
			if nil != launchErr {
				instance.events.EmitAsync(onError, launchErr.Error())
			}
		}
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *Updater) init(settings interface{}) {
	if nil != settings {
		if v, b := settings.(string); b {
			if lygo_regex.IsValidJsonObject(v) {
				// json string
				var s Settings
				err := lygo_json.Read(v, &s)
				if nil == err {
					instance.settings = &s
				}
			} else if b, _ := lygo_paths.Exists(v); b {
				// load from file
				text, err := lygo_io.ReadTextFromFile(v)
				if nil == err {
					instance.init(text)
				}
			}
		} else if v, b := settings.(Settings); b {
			instance.settings = &v
		}
	}
}

func (instance *Updater) initUpdaterEvents() {
	instance.events.On(onError, func(event *lygo_events.Event) {
		if nil != instance && nil != instance.errorHandlers {
			var err string
			item := event.Argument(0)
			if v, b := item.(string); b {
				err = v
			} else if v, b := item.(error); b {
				err = v.Error()
			}
			if len(err) > 0 {
				for _, handler := range instance.errorHandlers {
					if nil != handler {
						handler(err)
					}
				}
				instance.bubbleGenericEvent(onError, err)
			}
		}
	})
	instance.events.On(onUpgrade, func(event *lygo_events.Event) {
		if nil != instance && nil != instance.upgradeHandlers {
			arg1 := event.Argument(0)
			arg2 := event.Argument(1)
			arg3 := event.Argument(2)
			if fromVersion, b := arg1.(string); b {
				if toVersion, b := arg2.(string); b {
					files := lygo_conv.ToArrayOfString(arg3)
					for _, handler := range instance.upgradeHandlers {
						if nil != handler {
							handler(fromVersion, toVersion, files)
						}
					}

					instance.bubbleGenericEvent(onUpgrade, fromVersion, toVersion, files)
				}
			}
		}
	})
	instance.events.On(onTask, func(event *lygo_events.Event) {
		if nil != instance && nil != instance.taskHandlers {
			arg1 := event.Argument(0)
			if task, b := arg1.(*lygo_scheduler.SchedulerTask); b {
				uid := task.Uid
				payload := task.Payload
				for _, handler := range instance.taskHandlers {
					if nil != handler {
						handler(uid, payload)
					}
				}
				instance.bubbleGenericEvent(onTask, uid, payload)
			}
		}
	})
}

func (instance *Updater) startLauncher() (err error) {
	if nil != instance {
		// LAUNCH PROGRAM
		launcher := instance.getLauncher()
		if launcher.Pid() == -1 {
			// service was closed: run again
			err = instance.getLauncher().Run(replaceVars(instance.settings.CommandToRun, instance.variables))
		}
	}
	return
}

func (instance *Updater) stopLauncher() {
	if nil != instance && nil != instance.launcher {
		_ = instance.launcher.Kill() // STOP RUNNING PROGRAM
		instance.launcher = nil
	}
}

func (instance *Updater) getLauncher() *Launcher {
	if nil == instance.launcher {
		keepHandle := len(instance.settings.ScheduledUpdates) > 0
		instance.launcher = NewLauncher(keepHandle)
		instance.initLauncherEvents()
	}
	return instance.launcher
}

func (instance *Updater) initLauncherEvents() {
	if nil != instance.launcher {
		instance.launcher.OnStart(func(command string) {
			if nil != instance && nil != instance.launchStartHandlers {
				for _, callback := range instance.launchStartHandlers {
					callback(command)
				}
				instance.bubbleGenericEvent(onStart, command)
			}
		})
		instance.launcher.OnQuit(func(command string, pid int) {
			if nil != instance && nil != instance.launchQuitHandlers {
				for _, callback := range instance.launchQuitHandlers {
					callback(command, pid)
				}
				instance.bubbleGenericEvent(onQuit, command, pid)
			}
		})
	}
}

func (instance *Updater) bubbleGenericEvent(eventName string, args ...interface{}) {
	for _, handler := range instance.genericHandlers {
		if nil != handler {
			handler(instance, eventName, args)
		}
	}
}

func (instance *Updater) initScheduler() {

	// UPDATE
	if nil == instance.schedulerUpdate {
		// schedulerUpdate not already initialized
		if len(instance.settings.ScheduledUpdates) > 0 {
			instance.schedulerUpdate = lygo_scheduler.NewScheduler()
			for _, schedule := range instance.settings.ScheduledUpdates {
				instance.schedulerUpdate.AddSchedule(schedule)
			}
			instance.schedulerUpdate.OnError(func(error string) {
				instance.events.EmitAsync(onError, error)
			})
			instance.schedulerUpdate.OnSchedule(func(schedule *lygo_scheduler.SchedulerTask) {
				// fmt.Println("instance.schedulerUpdate.OnSchedule", schedule.String())
				_, _, _, _, _ = instance.Start() // check updates
			})
			instance.schedulerUpdate.Start()
		}
	}

	// RESTART
	if nil == instance.schedulerRestart {
		// schedulerUpdate not already initialized
		if len(instance.settings.ScheduledRestart) > 0 {
			instance.schedulerRestart = lygo_scheduler.NewScheduler()
			for _, schedule := range instance.settings.ScheduledRestart {
				instance.schedulerRestart.AddSchedule(schedule)
			}
			instance.schedulerRestart.OnError(func(error string) {
				instance.events.EmitAsync(onError, error)
			})
			instance.schedulerRestart.OnSchedule(func(schedule *lygo_scheduler.SchedulerTask) {
				instance.ReStart() // restart application
			})
			instance.schedulerRestart.Start()
		}
	}

	// TASKS
	if nil == instance.schedulerTask {
		// schedulerUpdate not already initialized
		if len(instance.settings.ScheduledTasks) > 0 {
			instance.schedulerTask = lygo_scheduler.NewScheduler()
			for _, schedule := range instance.settings.ScheduledTasks {
				instance.schedulerTask.AddSchedule(schedule)
			}
			instance.schedulerTask.OnError(func(error string) {
				instance.events.EmitAsync(onError, error)
			})
			instance.schedulerTask.OnSchedule(func(schedule *lygo_scheduler.SchedulerTask) {
				instance.events.EmitAsync(onTask, schedule)
			})
			instance.schedulerTask.Start()
		}
	}
}

func (instance *Updater) refreshVariables() {
	if nil != instance {
		instance.variables[VariableDirHome] = instance.root
		instance.variables[VariableDirStart] = instance.dirStart
		instance.variables[VariableDirApp] = instance.dirApp
		instance.variables[VariableDirWork] = instance.dirWork
	}
}

func (instance *Updater) checkUpdates() (updated bool, currentVersion string, remoteVersion string, files []string, err error) {
	if len(instance.settings.VersionFile) > 0 {
		updated, currentVersion, remoteVersion, files, err = instance.check()
	} else {
		err = lygo_errors.Prefix(ErrorMissingConfigurationParameter, "Missing Configuration Parameter 'VersionFile': ")
	}
	return updated, currentVersion, remoteVersion, files, err
}

func (instance *Updater) getVersions() (currentVersion string, remoteVersion string, filename string) {
	if nil != instance.settings && len(instance.settings.VersionFile) > 0 {
		url := instance.settings.VersionFile
		filename = lygo_paths.Concat(instance.root, lygo_paths.FileName(url, true))

		currentVersion = instance.getCurrentVersion(filename)
		remoteVersion = instance.getRemoteVersion(url)
	}
	return currentVersion, remoteVersion, filename
}

func (instance *Updater) check() (bool, string, string, []string, error) {
	currentVersion, remoteVersion, filename := instance.getVersions()
	needUpdate := instance.needUpdate(currentVersion, remoteVersion)
	files := make([]string, 0)

	if needUpdate {
		// download & install packages
		for _, v := range instance.settings.PackageFiles {
			source := v.File // may be an URL too.
			target := lygo_paths.Absolute(replaceVars(v.Target, instance.variables))
			err := instance.install(source, target)
			if nil != err {
				return false, currentVersion, remoteVersion, files, err
			} else {
				files = append(files, source, target)
			}
		}
	}

	if len(currentVersion) == 0 || needUpdate {
		// update version file
		_, err := lygo_io.WriteTextToFile(remoteVersion, filename)
		if nil != err {
			return false, currentVersion, remoteVersion, files, err
		}
	}

	return needUpdate, currentVersion, remoteVersion, files, nil
}

func (instance *Updater) getCurrentVersion(filename string) string {
	if s, err := lygo_io.ReadTextFromFile(filename); nil == err {
		return strings.Trim(s, " \n")
	}
	// file does not exists
	if instance.settings.VersionFileRequired {
		return "0"
	}
	return ""
}

func (instance *Updater) getRemoteVersion(filename string) string {
	data, err := instance.download(filename)
	if nil == err {
		return strings.Trim(string(data), " \n")
	}
	return ""
}

func (instance *Updater) install(url, target string) error {
	data, err := instance.download(url)
	if nil != err {
		return err
	}

	filename := lygo_paths.FileName(url, true)
	ext := lygo_paths.ExtensionName(filename)
	targetDir := lygo_paths.Absolute(target) + string(os.PathSeparator)
	err = lygo_paths.Mkdir(targetDir) // creates sub folders if missing
	if nil != err {
		return err
	}

	if "zip" == ext {
		// unzip
		tmp := lygo_rnd.Uuid() + ".zip"
		defer lygo_io.RemoveSilent(tmp)
		_, err := lygo_io.WriteBytesToFile(data, tmp)
		if nil != err {
			return err
		}
		_, err = lygo_io.Unzip(tmp, targetDir)
		if nil != err {
			return err
		}
	} else {
		// copy
		_, err := lygo_io.WriteBytesToFile(data, lygo_paths.Concat(targetDir, filename))
		if nil != err {
			return err
		}
	}

	return nil
}

func (instance *Updater) download(url string) ([]byte, error) {
	if len(url) > 0 {
		if strings.Index(url, "http") > -1 {
			// HTTP
			tr := &http.Transport{
				MaxIdleConns:       10,
				IdleConnTimeout:    15 * time.Second,
				DisableCompression: true,
			}
			client := &http.Client{Transport: tr}
			resp, err := client.Get(url)
			if nil == err {
				defer resp.Body.Close()
				body, err := ioutil.ReadAll(resp.Body)
				if nil == err {
					return body, nil
				} else {
					return []byte{}, err
				}
			} else {
				return []byte{}, err
			}
		} else {
			// FILE SYSTEM
			path := replaceVars(url, instance.variables)
			return lygo_io.ReadBytesFromFile(path)
		}
	}
	return []byte{}, lygo_errors.Prefix(ErrorMissingConfigurationParameter, "Missing Configuration Parameter 'VersionFile': ")
}

func (instance *Updater) needUpdate(currentVersion, remoteVersion string) bool {
	if len(currentVersion) > 0 && len(remoteVersion) > 0 && currentVersion != remoteVersion {
		v1 := strings.Split(strings.Trim(currentVersion, " \n"), ".")
		v2 := strings.Split(strings.Trim(remoteVersion, " \n"), ".")
		for i := 0; i < 3; i++ {
			a := lygo_conv.ToInt(lygo.Arrays.GetAt(v1, i, 0))
			b := lygo_conv.ToInt(lygo.Arrays.GetAt(v2, i, 0))
			if b == a {
				continue
			} else if b < a {
				return false
			} else if b > a {
				return true
			}
		}
	}
	return false
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

func replaceVars(text string, variables map[string]string) string {
	txt := text
	for k, v := range variables {
		txt = strings.Replace(txt, k, v, -1)
	}
	return txt
}
