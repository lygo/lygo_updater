package _test

import (
	"bitbucket.org/lygo/lygo_updater/src"
	"fmt"
	"testing"
	"time"
)

func TestLauncher(t *testing.T) {
	command := "./versions_install/test_fiber"
	launcher := lygo_updater.NewLauncher(true)
	launcher.OnStart(func(command string) {
		fmt.Println("START EVENT!")
	})
	launcher.OnQuit(func(command string, pid int) {
		fmt.Println("QUIT EVENT!")
	})

	err := launcher.Run(command)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	fmt.Println("PID", launcher.Pid())
	fmt.Println("INFO", launcher)

	time.Sleep(3 * time.Second)

	fmt.Println(launcher.Output())

	if launcher.IsKillable() {
		err = launcher.Kill()
		if nil != err {
			t.Error(err)
			t.FailNow()
		}
	} else {
		fmt.Println("NOT 'KILLABLE' PROCESS")
	}
}

func TestLauncherNotes(t *testing.T) {
	command := "open -a Notes"
	launcher := lygo_updater.NewLauncher(true)
	launcher.OnStart(func(command string) {
		fmt.Println("START EVENT!")
	})
	launcher.OnQuit(func(command string, pid int) {
		fmt.Println("QUIT EVENT!")
	})

	err := launcher.Run(command)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	fmt.Println("PID", launcher.Pid())
	fmt.Println("INFO", launcher)

	time.Sleep(3 * time.Second)

	if launcher.IsKillable() {
		fmt.Println(launcher.Output())

		err = launcher.Kill()
		if nil != err {
			t.Error(err)
			t.FailNow()
		}
	} else {
		fmt.Println("NOT 'KILLABLE' PROCESS")
	}
}
