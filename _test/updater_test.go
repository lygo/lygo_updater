package _test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_updater/src"
	"fmt"
	"testing"
)

func TestUpdater(t *testing.T) {
	app := lygo_updater.NewUpdater()
	app.OnUpgrade(func(fromVersion, toVersion string, files []string) {
		fmt.Println("OnUpgrade", fromVersion, toVersion)
	})
	app.OnError(func(err string) {
		fmt.Println("OnError", err)
	})
	app.OnLaunchStart(func(command string) {
		fmt.Println("OnLaunchStart", command)
	})
	app.OnLaunchQuit(func(command string, pid int) {
		fmt.Println("OnLaunchQuit", command, pid)
	})
	app.OnEvent(func(updater *lygo_updater.Updater, eventName string, args []interface{}) {
		fmt.Println(updater.GetUid(), eventName, args)
	})

	updated, from, to, files, err := app.Start()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("updated", updated, from, to, files)
}

func TestVersion(t *testing.T) {

	app := lygo_updater.NewUpdater()
	app.Settings().VersionFileRequired = false

	if app.IsUpgradable("", "1.1.1") {
		t.Error("expected false")
		t.FailNow()
	}

	if !app.IsUpgradable("0", "1.1.1") {
		t.Error("expected true")
		t.FailNow()
	}

	if app.IsUpgradable("1.0.0", "0") {
		t.Error("expected false")
		t.FailNow()
	}

	if !app.IsUpgradable("1.0.0", "1.1.1") {
		t.Error("expected true")
		t.FailNow()
	}

	if app.IsUpgradable("2.0.0", "1.1.1") {
		t.Error("expected false")
		t.FailNow()
	}
	if app.IsUpgradable("1.2", "1.1.1") {
		t.Error("expected false")
		t.FailNow()
	}
	if !app.IsUpgradable("1.2", "1.2.1") {
		t.Error("expected true")
		t.FailNow()
	}

}

func TestUpdaterLocal(t *testing.T) {
	// reset local version file
	_, err := lygo_io.WriteTextToFile("1.0.0", "./version.txt")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	updater := lygo_updater.NewUpdater()
	updater.Settings().VersionFile = "./versions/version.txt"
	updater.Settings().PackageFiles = make([]*lygo_updater.PackageFile, 0)
	updater.Settings().PackageFiles = append(updater.Settings().PackageFiles, &lygo_updater.PackageFile{
		File:   "./versions/test_fiber.zip",
		Target: "./versions_install",
	})
	updater.Settings().CommandToRun = ""

	updated, from, to, files, err := updater.Start()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("updated", updated, from, to, files)

	updater.Stop()
}
