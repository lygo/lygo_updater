package _test

import (
	lygo_updater "bitbucket.org/lygo/lygo_updater/src"
	"fmt"
	"testing"
	"time"
)

func TestTasks(t *testing.T) {
	updater := lygo_updater.NewUpdater("./updater-tasks.json")

	updater.OnError(func(err string) {
		fmt.Println("ERROR", err)
	})


	updater.OnLaunchStart(func(command string) {
		fmt.Println("OnLaunchStart", command)
	})
	updater.OnLaunchQuit(func(command string, pid int) {
		fmt.Println("\tOnLaunchQuit", command, pid)
	})
	updater.OnTask(func(taskUID string, payload map[string]interface{}) {
		fmt.Println("\tOnTask", taskUID, payload)
	})

	_, _, _, _, _ = updater.Start()

	// wait a while to allow checks
	time.Sleep(30*time.Second)

	fmt.Println("BEFORE STOP", "pid:", updater.GetProcessPid())
	fmt.Println(updater.GetProcessOutput())

	updater.Stop() // kill process before exit
	fmt.Println("AFTER STOP", "pid:", updater.GetProcessPid())
	fmt.Println(updater.GetProcessOutput())
}
