package _test

import (
	lygo_updater "bitbucket.org/lygo/lygo_updater/src"
	"fmt"
	"testing"
	"time"
)

func TestRestart(t *testing.T) {
	updater := lygo_updater.NewUpdater("./updater-restart.json")
	updater.Settings().VersionFile = "./versions/version.txt"
	updater.Settings().PackageFiles = make([]*lygo_updater.PackageFile, 0)
	updater.Settings().PackageFiles = append(updater.Settings().PackageFiles, &lygo_updater.PackageFile{
		File:   "./versions/test_fiber.zip",
		Target: "./versions_install",
	})
	updater.Settings().CommandToRun = "./versions_install/test_fiber"
	updater.OnError(func(err string) {
		fmt.Println("ERROR", err)
	})
	count:=0
	updater.OnUpgrade(func(fromVersion, toVersion string, files []string) {
		count++
		now := time.Now()
		t := fmt.Sprintf("%v:%v:%v", now.Hour(), now.Minute(), now.Second())
		fmt.Println("UPDATE", count, t, "\t" + fromVersion + " -> " + toVersion, "pid:", updater.GetProcessPid())
	})

	updater.OnLaunchStart(func(command string) {
		fmt.Println("OnLaunchStart", command)
	})
	updater.OnLaunchQuit(func(command string, pid int) {
		fmt.Println("\tOnLaunchQuit", command, pid)
	})

	_, _, _, _, _ = updater.Start()

	// wait a while to allow checks
	time.Sleep(30*time.Second)

	fmt.Println("BEFORE STOP", "pid:", updater.GetProcessPid())
	fmt.Println(updater.GetProcessOutput())

	updater.Stop() // kill process before exit
	fmt.Println("AFTER STOP", "pid:", updater.GetProcessPid())
	fmt.Println(updater.GetProcessOutput())
}
